extends PopupMenu

@export var colors: Array[ColorSetting] = Array()

var last_checked: int = 0

func _ready() -> void:
	for i in colors.size():
		colors[i].test()
		add_item(colors[i].name)
		set_item_as_radio_checkable(i, true)
	_on_index_pressed(0)


func _on_index_pressed(index: int) -> void:
	Overlay.colors = colors[index]
	set_item_checked(last_checked, false)
	set_item_checked(index, true)
	last_checked = index
