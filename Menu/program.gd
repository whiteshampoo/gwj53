extends PopupMenu

signal run
signal reset
signal step
signal input_break

enum {
	RUN,
	RESET,
	_SEPERATOR_,
	STEP,
	BREAK,
}

func set_state(state: CPU.STATE) -> void:
	match state:
		CPU.STATE.RUNNING:
			set_item_disabled(RUN, true)
			set_item_disabled(RESET, false)
			#set_item_disabled(STEP, false)
		CPU.STATE.STEPPING:
			set_item_disabled(RUN, false)
			set_item_disabled(RESET, false)
			#set_item_disabled(STEP, false)
		_: # DEVELOPING
			set_item_disabled(RUN, false)
			set_item_disabled(RESET, true)
			#set_item_disabled(STEP, false)
	


func _on_id_pressed(id: int) -> void:
	print(id)
	match id:
		RUN:
			run.emit()
		RESET:
			reset.emit()
		STEP:
			step.emit()
		BREAK:
			set_item_checked(BREAK, not is_item_checked(BREAK))
			input_break.emit()
