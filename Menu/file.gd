extends PopupMenu


func _on_index_pressed(index: int) -> void:
	match index:
		0:
			OS.shell_open(OS.get_user_data_dir())
		1:
			get_tree().quit()
