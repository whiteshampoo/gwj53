extends PopupMenu

signal instructions
signal credits

enum {
	INSTRUCTIONS,
	CREDITS,
}

func _on_index_pressed(index: int) -> void:
	match index:
		INSTRUCTIONS:
			instructions.emit()
		CREDITS:
			credits.emit()
