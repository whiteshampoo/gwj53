extends PopupMenu

enum {
	WINDOWED,
	FULLSCREEN,
}

const FULLSCREEN_MODES: Array = [
	DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN,
	DisplayServer.WINDOW_MODE_FULLSCREEN,
]

func _on_about_to_popup() -> void:
	var is_fullscreen: bool = DisplayServer.window_get_mode(0) in FULLSCREEN_MODES
	set_item_checked(FULLSCREEN, is_fullscreen)
	set_item_checked(WINDOWED, not is_fullscreen)


func _on_index_pressed(index: int) -> void:
	match index:
		WINDOWED:
			Constants.set_windowed()
		FULLSCREEN:
			Constants.set_fullscreen()
