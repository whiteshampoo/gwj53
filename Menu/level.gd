extends PopupMenu

signal change_level(path)

var last_index: int = 0

func _on_index_pressed(index: int) -> void:
	set_item_checked(last_index, false)
	set_item_checked(index, true)
	last_index = index
	var level: String = Const.LEVEL_FOLDER + get_item_text(index).replace(" ", "_") + ".tscn"
	level = level.to_lower()
	#assert(FileAccess.file_exists(level), level)
	change_level.emit(level)
