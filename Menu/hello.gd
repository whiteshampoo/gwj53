extends Window

signal closed

func _on_close_requested() -> void:
	closed.emit()
	queue_free()
