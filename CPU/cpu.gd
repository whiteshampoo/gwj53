class_name CPU
extends Control

signal A_changed(new_value)
signal B_changed(new_value)
signal P_changed(new_value)
signal S_changed(new_value)
signal memory_changed(new_bytes)
signal output_changed(new_outputs)
signal input_changed(new_inputs)

signal stepped

enum STATE {
	DEVELOPING,
	RUNNING,
	STEPPING,
}

enum {
	ERROR = -1,
	CMD,
	ARG,
	LABELS,
}

enum STATUS {
	TEST,
	STACK = 4,
}

const INSTRUCTIONS: Array = [
	"SETA",	"SETB",	"LDA", "LDB", "STA", "STB", "SWP",
	"ADD", "SUB", "DIV", "MUL", "MOD", "SHL", "SHR", "CMB", "SPL",
	"AND", "OR", "NAND", "NOR", "XOR", "XNOR", "NOT",
	"JMP", "NOP", "INA", "INB", "OUTA", "OUTB", "SKIP", "RND", "NULL",
	"PUSH", "POPA", "POPB", "CALL", "RET", "ZERO", 
	"TEQ", "TNE", "TL", "TLE", "TG", "TGE",
]

const EXT_INST: Array = [
	"SKIP", ""
]

const CHARS: PackedStringArray = [
	"A", "B", "C", "D", "E", "F", "G",
	"H", "I", "J", "K", "L", "M", "N",
	"O", "P", "Q", "R", "S", "T", "U",
	"V", "W", "X", "Y", "Z", "_",
]

const REGISTERS: PackedStringArray = ["A", "B", "P", "S"]


var state: STATE = STATE.DEVELOPING
var steps: int = 0
var time: float = 0
var hz: float = 0.025

var A: int = 0:
	set(a):
		A = a & 0xFF
		A_changed.emit(A)
		update_register_label()

var B: int = 0:
	set(b):
		B = b & 0xFF
		B_changed.emit(B)
		update_register_label()

var S: int = 0: # Status
	set(s):
		S = s & 0xFF
		S_changed.emit(S)
		update_register_label()

var P: int = 0: # Line-Pointer
	set(p):
		if instruction_lines.size() == 0:
			return
		if p >= instruction_lines.size():
			p = 0
		while(p in labels.values() or instruction_lines.get(p, {}).get(CMD, null) == SKIP):
			p += 1 # refactor me
		if p >= instruction_lines.size():
			p = 0
		P = p & 0xFF
		P_changed.emit(P)
		update_register_label()

var memory: PackedByteArray = [0, 0, 0, 0, 0, 0, 0, 0]:
	set(new_memory):
		new_memory.resize(8)
		memory = new_memory
		update_memory_label()
var inputs: PackedByteArray = [0, 0]
var outputs: PackedByteArray = [0, 0]
var instruction_lines: Dictionary = Dictionary()
var labels: Dictionary = Dictionary()

var instructions: Dictionary = Dictionary()

@onready var Registers: Label = $Div/Internal/Div/Values
@onready var Memory: Label = $Div/Ram/Div/Values

func _ready() -> void:
	update_memory_label()
	for method in get_method_list():
		if method["name"] in INSTRUCTIONS:
			instructions[method["name"]] = method["args"].size()


func _process(delta: float) -> void:
	if not state == STATE.RUNNING:
		time = 0.0
		return
	time += delta
	while time > hz:
		time -= hz
		step()

func reset() -> void:
	ZERO()
	P = 0
	state = STATE.DEVELOPING
	outputs = [0, 0]
	inputs = [0, 0]
	memory = [0, 0, 0, 0, 0, 0, 0, 0]
	steps = 0
	labels = Dictionary()
	instruction_lines = Dictionary()

func update_register_label() -> void:
	Registers.text = "%02X\n" % inputs[0]
	Registers.text += "%02X\n" % inputs[1]
	Registers.text += "%02X\n" % outputs[0]
	Registers.text += "%02X\n" % outputs[1]
	Registers.text += "%02X\n" % A
	Registers.text += "%02X\n" % B
	Registers.text += "%02X\n" % P
	Registers.text += "%02X" % S


func update_memory_label() -> void:
	Memory.text = "%02X\n" % memory[0]
	Memory.text += "%02X\n" % memory[1]
	Memory.text += "%02X\n" % memory[2]
	Memory.text += "%02X\n" % memory[3]
	Memory.text += "%02X\n" % memory[4]
	Memory.text += "%02X\n" % memory[5]
	Memory.text += "%02X\n" % memory[6]
	Memory.text += "%02X\n" % memory[7]



func step() -> void:
	if instruction_lines.size() == 0:
		return
	if P >= instruction_lines.size():
		P = 0
	var line: Dictionary = instruction_lines[P].duplicate()
	P += 1
	steps += 1
	if check_status(STATUS.TEST):
		line[CMD] = NOP
		line[ARG] = ""
		set_status(STATUS.TEST, false)
	var cmd: Callable = line[CMD]
	if line[ARG] is int:
		var arg: int = line[ARG]
		#prints(cmd, arg)
		cmd.call(arg)
		stepped.emit()
		return
	elif line[ARG] is String:
		var arg: int = 0
		var sarg: String = line[ARG]
		if sarg == "":
			#print(cmd)
			cmd.call()
			stepped.emit()
			return
		assert(sarg in REGISTERS)
		if not sarg in REGISTERS:
			arg = 0
		else:
			arg = get(sarg)
		#prints(cmd, arg)
		cmd.call(arg)
		stepped.emit()
		return
	@warning_ignore(assert_always_false)
	assert(false)

func parse_asm(asm: String) -> Dictionary:
	asm = asm.strip_edges(false, true).replace("\t", " ")
	var tmp: String = ""
	while(true):
		tmp = asm.replace("  ", " ")
		if tmp == asm:
			break
		asm = tmp
	
	var output: Dictionary = Dictionary()
	
	var lines: PackedStringArray = asm.split("\n")
	for i in lines.size():
		lines[i] = lines[i].split("#")[0]
	var new_labels: Dictionary = find_labels(lines)
	if ERROR in new_labels:
		return new_labels
	output[LABELS] = new_labels
	
	var new_commands: Dictionary = parse_lines(lines, new_labels)
	if ERROR in new_commands:
		return new_commands
	output[CMD] = new_commands
	return output

func find_labels(lines: PackedStringArray) -> Dictionary:
	var output: Dictionary = Dictionary()
	for i in lines.size():
		var line: String = lines[i].strip_edges()
		if line == "":
			continue
		if line.count(" ") != 0:
			continue
		if line.ends_with(":") and line.count(":") == 1:
			line = line.replace(":", "")
			if line in REGISTERS or line in instructions or line in output:
				return {ERROR: "Line %d: Label Error" % i}
			for c in line:
				if not c in CHARS:
					return {ERROR: "Line %d: Label Error" % i}
			output[line] = i
	return output


func parse_lines(lines: PackedStringArray, new_labels: Dictionary) -> Dictionary:
	var output: Dictionary = Dictionary()
	var only_skips: bool = true
	for i in lines.size():
		var line: String = lines[i].strip_edges()
		if line == "" or i in new_labels.values():
			line = "SKIP"
		var parts: PackedStringArray = line.split(" ")
		var command: String = parts[CMD]
		if not command in instructions:
			return {ERROR: "Line %d: Instruction Error" % i}
		if not parts.size() - 1 == instructions[command]:
			return {ERROR: "Line %d: Argument Error" % i}
		if command != "SKIP":
			only_skips = false
		output[i] = {
			CMD: Callable(self, command),
			ARG: "",
		}
		if parts.size() == 2:
			var arg: String = parts[ARG]
			if arg in REGISTERS:
				output[i][ARG] = arg
				continue
			if arg in new_labels:
				output[i][ARG] = new_labels[arg]
				continue
			if arg.is_valid_int():
				output[i][ARG] = arg.to_int()
				continue
			if arg.is_valid_hex_number(true):
				output[i][ARG] = arg.hex_to_int()
				continue
			return {ERROR: "Line %d: Argument Error" % i}
	if only_skips:
		return {ERROR: "No Instructions Error"}
	return output


func set_input(input: int, value: int) -> void:
	var old_hash: int = Array(inputs).hash()
	inputs[input % inputs.size()] = value & 0xFF
	if old_hash != Array(inputs).hash():
		input_changed.emit(inputs)
	update_register_label()
	


func set_input_bit(input: int, bit: int, value: bool) -> void:
	if value:
		set_input(input, inputs[input] | 1 << bit)
	else:
		set_input(input, inputs[input] & ~(1 << bit))

func set_status(bit: int, value: bool) -> void:
	if value:
		S |= 1 << bit
	else:
		S &= ~(1 << bit)
	update_register_label()


func check_status(bit : int) -> bool:
	return bool(S & (1 << bit))


func get_status_stack() -> int:
	#print((S >> STATUS.STACK) & 0b00000111)
	return (S >> STATUS.STACK) & 0b00000111


func set_status_stack(value: int) -> void:
	S &= ~(0b00000111 << STATUS.STACK)
	S |= (value & 0b00000111) << STATUS.STACK
	update_register_label()


func SETA(arg: int) -> void:
	A = arg


func SETB(arg: int) -> void:
	B = arg


func LDA(arg: int) -> void:
	A = memory[arg % memory.size()]


func LDB(arg: int) -> void:
	B = memory[arg % memory.size()]


func STA(arg: int) -> void:
	memory[arg % memory.size()] = A
	update_memory_label()

func STB(arg: int) -> void:
	memory[arg % memory.size()] = B
	update_memory_label()


func SWP() -> void:
	var C: int = A
	A = B
	B = C


func ADD(arg: int) -> void:
	A += arg


func SUB(arg: int) -> void:
	A -= arg


func DIV(arg: int) -> void:
	A /= arg


func MUL(arg: int) -> void:
	A *= arg


func MOD(arg: int) -> void:
	A %= arg


func SHL(arg: int) -> void:
	A <<= arg


func SHR(arg: int) -> void:
	A >>= arg


func CMB() -> void:
	A &= 0x0F
	A |= (B & 0x0F) << 4

func SPL() -> void:
	B = (A >> 4)
	A &= 0x0F

func AND(arg: int) -> void:
	A &= arg


func OR(arg: int) -> void:
	A |= arg


func NAND(arg: int) -> void:
	A = ~(A & arg)


func NOR(arg: int) -> void:
	A = ~(A | arg)


func XOR(arg: int) -> void:
	A ^= arg


func XNOR(arg: int) -> void:
	A = ~(A ^ arg)


func NOT() -> void:
	A = ~A


func JMP(arg: int) -> void:
	P = arg


func NOP() -> void:
	pass


func INA(arg: int) -> void:
	A = inputs[arg % inputs.size()]


func INB(arg: int) -> void:
	B = inputs[arg % inputs.size()]


func OUTA(arg: int) -> void:
	outputs[arg % outputs.size()] = A
	output_changed.emit(outputs)


func OUTB(arg: int) -> void:
	outputs[arg % outputs.size()] = B
	output_changed.emit(outputs)


func SKIP() -> void:
	step()


func RND() -> void:
	A = randi() & 0xFF

func NULL() -> void:
	NULL()
	P = 0
	outputs = [0, 0]
	output_changed.emit(outputs)


func PUSH(arg: int) -> void:
	memory[memory.size() - get_status_stack() - 1] = arg & 0xFF
	set_status_stack(get_status_stack() + 1)
	update_memory_label()


func POPA() -> void:
	set_status_stack(get_status_stack() - 1)
	A = memory[memory.size() - get_status_stack() - 1]
	


func POPB() -> void:
	set_status_stack(get_status_stack() - 1)
	B = memory[memory.size() - get_status_stack() - 1]
	


func CALL(arg: int) -> void:
	PUSH(P)
	P = arg
	update_memory_label()


func RET() -> void:
	set_status_stack(get_status_stack() - 1)
	P = memory[memory.size() - get_status_stack() - 1]
	


func ZERO() -> void:
	A = 0
	B = 0
	S = 0



func TEQ(arg: int) -> void:
	set_status(STATUS.TEST, not A == arg)


func TNE(arg: int) -> void:
	set_status(STATUS.TEST, not A != arg)


func TL(arg: int) -> void:
	set_status(STATUS.TEST, not A < arg)


func TLE(arg: int) -> void:
	set_status(STATUS.TEST, not A <= arg)


func TG(arg: int) -> void:
	set_status(STATUS.TEST, not A > arg)


func TGE(arg: int) -> void:
	set_status(STATUS.TEST, not A >= arg)


