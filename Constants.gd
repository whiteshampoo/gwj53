class_name Const
extends Node2D

const COLORS: PackedColorArray = [
	Color(0.000, 0.000, 0.000),
	Color(0.333, 0.333, 0.333),
	Color(0.666, 0.666, 0.666),
	Color(1.000, 1.000, 1.000),
]

const LEVEL_FOLDER: String = "res://level/"

var mode: DisplayServer.WindowMode = DisplayServer.WINDOW_MODE_WINDOWED

func set_fullscreen() -> void:
	mode = DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN
	DisplayServer.window_set_mode(mode)
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED_HIDDEN


func set_windowed() -> void:
	mode = DisplayServer.WINDOW_MODE_WINDOWED
	DisplayServer.window_set_mode(mode)
	Input.mouse_mode = Input.MOUSE_MODE_HIDDEN


func toggle_fullscreen() -> void:
	if mode == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN:
		set_windowed()
	else:
		set_fullscreen()

func _process(_delta: float) -> void:
#	var pos: Vector2i = get_global_mouse_position()
#	if mode == DisplayServer.WINDOW_MODE_EXCLUSIVE_FULLSCREEN and pos.y < 1:
#		Input.warp_mouse(pos + Vector2i(1, 1))
	if Input.is_action_just_pressed("fullscreen"):
		toggle_fullscreen()
			
