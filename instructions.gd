extends RichTextLabel

signal back

const LINE_END: String = "[/color]\n"
const HEADLINE: String = "[color=#555]"
const NORMAL: Array = ["[color=#FFF]", "[color=#AAA]"]


func _ready() -> void:
	var lines: PackedStringArray = text.split("\n")
	text = ""
	var count: int = 0
	for line in lines:
		if line in ["[center]", "[/center]"]:
			text += line + "\n"
			continue
		if line == "":
			text += "\n"
			continue
		if line.ends_with(":"):
			text += HEADLINE + line + LINE_END
			count = 0
			continue
		if line.begins_with("\t"):
			count -= 1
		text += NORMAL[count % 2] + line + LINE_END
		count += 1


func _on_back_pressed() -> void:
	back.emit()


func _on_meta_clicked(meta: String) -> void:
	assert(meta.begins_with("https://"))
	OS.shell_open(meta)
