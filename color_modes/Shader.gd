extends ColorRect

signal color_changed

const FAIL_COLOR: ColorSetting = preload("res://color_modes/fail.tres")

@export var colors: ColorSetting = FAIL_COLOR:
	set(new_colors):
		if not new_colors:
			new_colors = FAIL_COLOR
		colors = new_colors
		for i in 4:
			material.set_shader_parameter("color_%d" % i, colors.colors[i])
		color_changed.emit(colors)


# Having multiple arrows is not nice and i dont care
@onready var Arrow: Sprite2D = $Arrow


func _ready() -> void:
	if Engine.is_editor_hint():
		hide()
		return
	show()
	#hide()
	if self == Overlay:
		Input.mouse_mode = Input.MOUSE_MODE_HIDDEN
		return
	Overlay.color_changed.connect(set_colors)


func _process(_delta: float) -> void:
	Arrow.global_position = get_global_mouse_position().round()



func set_colors(new_colors: ColorSetting) -> void:
	colors = new_colors

