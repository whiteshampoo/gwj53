class_name ColorSetting
extends Resource

@export var name: String = ""
@export var colors: Array[Color] = []

func test() -> void:
	assert(name)
	assert(colors.size() == 4)
