extends OptionButton

@export var colors: Array[ColorSetting] = Array()

func _ready() -> void:
	for color in colors:
		color.test()
		add_item(color.name)
		get_selected_id()
	Overlay.colors = colors[get_selected_id()]
	var child: PopupMenu = get_child(0, true) as PopupMenu
	assert(is_instance_valid(child), "Child is not a PopupMenu")
	child.add_child(Overlay.duplicate())

func _on_item_selected(index: int) -> void:
	Overlay.colors = colors[index]
