@tool
extends CodeEdit

var last_line: int = -1
var last_text: String = ""
var max_lines = 20
var ignore_input: bool = false:
	set(new_ignore_input):
		ignore_input = new_ignore_input
		last_text = text
		if ignore_input:
			caret_type = TextEdit.CARET_TYPE_LINE
			set_caret_column(0)
			set_caret_line(0)
		else:
			caret_type = TextEdit.CARET_TYPE_BLOCK
			set_caret_column(0)

@onready var LineNumbers: Label = $LineNumbers

func _ready() -> void:
	last_text = text
	var keywords: Dictionary = syntax_highlighter.keyword_colors
	for instruction in CPU.INSTRUCTIONS:
		if not instruction in CPU.EXT_INST:
			keywords[instruction] = Color(0.666, 0.666, 0.666)
	update_line_numbers()

func set_p_line(line: int) -> void:
	#editable = false
	set_caret_line(line)
	highlight_current_line = true
	last_line = line

func update_line_numbers() -> void:
	var lines: int = text.count("\n")
	LineNumbers.text = ""
	for i in lines + 1:
		LineNumbers.text += "%02d\n" % i

func to_upper() -> void:
	var line: int = get_caret_line()
	var column: int = get_caret_column()
	text = text.to_upper().replace("0X", "0x")
	set_caret_line(line)
	set_caret_column(column)

func _on_text_changed() -> void:
	if ignore_input:
		text = last_text
		set_caret_line(last_line)
	call_deferred("update_line_numbers")
	var lines: PackedStringArray = text.split("\n")
	var line_changed: bool = false
	for i in lines.size():
		if lines[i].length() > 20:
			lines[i] = lines[i].left(20)
			line_changed = true
	if lines.size() <= max_lines and not line_changed:
		last_text = text
		return
	var line: int = get_caret_line()
	var column: int = get_caret_column()
	text = last_text
	#custom_minimum_size.y = lines.size() * 12 - 2 # shotgun
	if line_changed:
		text = "\n".join(lines)
		last_text = text
	set_caret_line(line)
	set_caret_column(column)


func load_code(file_name: String) -> void:
	file_name = file_name.trim_prefix(Const.LEVEL_FOLDER).trim_suffix(".tscn")
	print("load: ", file_name)
	if not FileAccess.file_exists("user://" + file_name + ".txt"):
		text = ""
		update_line_numbers()
		return
	var file: FileAccess = FileAccess.open("user://" + file_name + ".txt", FileAccess.READ)
	var new_text: String = file.get_as_text()
	var lines: PackedStringArray = new_text.split("\n")
	if lines.size() > max_lines:
		lines.resize(max_lines)
	for i in lines.size():
		lines[i] = lines[i].left(20).to_upper().replace("0X", "0x")
	text = "\n".join(lines)
	last_text = text
	update_line_numbers()


func save_code(file_name: String) -> void:
	file_name = file_name.trim_prefix(Const.LEVEL_FOLDER).trim_suffix(".tscn")
	print("save: ", file_name)
	var file: FileAccess = FileAccess.open("user://" + file_name + ".txt", FileAccess.WRITE)
	file.store_string(text)


func _on_caret_changed() -> void:
	var mid: int = get_caret_line() * 12
	var scroll: float = get_parent().scroll_vertical
	var relative: float = mid - scroll
	if relative < 0:
		get_parent().scroll_vertical = mid
	if relative > 228:
		get_parent().scroll_vertical = mid - 228
	
	if not ignore_input:
		return
	set_caret_column(0)
	set_caret_line(last_line)
