extends Label

func _ready() -> void:
	text = ""



func add_line(line: String) -> void:
	if text == "":
		text = line
		return
	line = line.left(28)
	var lines: PackedStringArray = text.split("\n")
	while lines.size() > 4:
		lines.remove_at(0)
	lines.append(line)
	text = "\n".join(lines)
