extends OutputModule

@onready var Blades: TextureRect = $Blades

var speed: float = 0

func _process(delta: float) -> void:
	var target_speed: float = float(Cpu.outputs[output])
	speed = lerp(speed, target_speed, delta * 2.0)
	if abs(speed - target_speed) < 0.5:
		speed = target_speed
	Blades.rotation += (speed / 16.0) * delta
