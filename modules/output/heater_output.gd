extends OutputModule

@onready var Heat: ColorRect = $Heat

var heat: float = 0.0

func _process(delta: float) -> void:
	var target_heat: float = float(Cpu.outputs[output])
	heat = lerp(heat, target_heat, delta * 2.0)
	if abs(heat - target_heat) < 0.5:
		heat = target_heat
	Heat.size.y = int(remap(heat, 0.0, 255.0, 0.0, 28.0))
	Heat.position.y = 44 - Heat.size.y
