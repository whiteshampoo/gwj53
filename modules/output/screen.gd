extends IOModule

enum MODE {
	READ_INSTRUCTION,
	READ_WAIT_XY,
	READ_XY,
	READ_WAIT_COLOR,
	READ_COLOR,
	END_WAIT,
}

enum INSTRUCTIONS {
	WAIT,
	SET_PIXEL,
	GET_PIXEL,
	FILL,
	MAX,
}

const COLORS: PackedColorArray = Const.COLORS


@onready var TR: TextureRect = $TextureRect
var img: Image = Image.create(16, 16, false, Image.FORMAT_RGBA8)
var tex: ImageTexture = ImageTexture.create_from_image(img)

var x: int = 0
var y: int = 0
var mode: MODE = MODE.READ_INSTRUCTION
var instruction: INSTRUCTIONS = INSTRUCTIONS.WAIT

func reset():
	init()

func init() -> void:
	img.fill(Color.BLACK)
	tex.update(img)
	TR.texture = tex
	if not Cpu.stepped.is_connected(step):
		Cpu.stepped.connect(step)
	mode = MODE.READ_INSTRUCTION
	instruction = INSTRUCTIONS.WAIT

func test_colors(c1: Color, c2: Color) -> bool:
	return \
		abs(c1.r - c2.r) < 0.1 and \
		abs(c1.g - c2.g) < 0.1 and \
		abs(c1.b - c2.b) < 0.1

func step() -> void:
	match mode:
		MODE.READ_INSTRUCTION:
			instruction = (read_input() % INSTRUCTIONS.MAX) as INSTRUCTIONS
			match instruction:
				INSTRUCTIONS.SET_PIXEL, INSTRUCTIONS.GET_PIXEL:
					mode = MODE.READ_WAIT_XY
				INSTRUCTIONS.FILL:
					mode = MODE.READ_WAIT_COLOR
				_:
					mode = MODE.READ_INSTRUCTION
		MODE.READ_WAIT_XY:
			mode = MODE.READ_XY
		MODE.READ_XY:
			x = ((read_input() & 0x0F) >> 0) % img.get_width()
			y = ((read_input() & 0xF0) >> 4) % img.get_height()
			mode = MODE.READ_WAIT_COLOR
		MODE.READ_WAIT_COLOR:
			mode = MODE.READ_COLOR
		MODE.READ_COLOR:
			match instruction:
				INSTRUCTIONS.SET_PIXEL:
					img.set_pixel(x, y, COLORS[read_input() % COLORS.size()])
					tex.update(img)
				INSTRUCTIONS.GET_PIXEL:
					var pixel: Color = img.get_pixel(x, y)
					for i in COLORS.size():
						if test_colors(pixel, COLORS[i]):
							write_output(i)
							break
				INSTRUCTIONS.FILL:
					img.fill(COLORS[read_input() % COLORS.size()])
					tex.update(img)
			mode = MODE.END_WAIT
		MODE.END_WAIT:
			mode = MODE.READ_INSTRUCTION


func read_input() -> int:
	return Cpu.outputs[output]

func write_output(arg: int) -> void:
	Cpu.set_input(input, arg)
