extends OutputModule

@onready var Outputs: Array = [
	$"Outputs/0",
	$"Outputs/1",
	$"Outputs/2",
	$"Outputs/3"
]

func reset():
	for i in Outputs.size():
		Outputs[i].color = Const.COLORS[0]


func init() -> void:
	Cpu.output_changed.connect(output_changed)


func output_changed(outputs: Array) -> void:
	for i in Outputs.size():
		Outputs[i].color = Const.COLORS[(outputs[output] >> (i * 2)) & 0b00000011]
