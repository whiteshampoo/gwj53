class_name OutputModule
extends IOModule

const TEX_OK: int = 7
const TEX_NOT_OK: int = 14
const TEX_FAIL: int = 21

@export var ignore_requirements: bool = false
@export var output_name: String = "Name Me!"
#@export var timeout: int = 40
@export var max_requirement: int = 0xFF
@export var min_requirement: int = 0x00
var steps: int = 0
var blink: bool = false
var failed: bool = false
@onready var Indicator: TextureRect = $Indicator
@onready var Symbol: TextureRect = $Indicator/Symbol

func _ready() -> void:
	reset()


func reset() -> void:
	max_requirement = 0xFF
	min_requirement = 0x00
	steps = 0
	blink = false
	failed = false
	Symbol.texture.region.position.x = TEX_OK
	Symbol.hide()


func step() -> void:
	if failed:
		return
	if max_requirement == 0xFF and min_requirement == 0x00:
		steps = 0
		#Indicator.hide()
		Symbol.hide()
		return
	Indicator.show()
	if _test():
		#prints(name, "ok")
		steps = 0
		Symbol.texture.region.position.x = TEX_OK
		Symbol.show()
		blink = false
		return
	#prints(name, "not ok")
	Symbol.texture.region.position.x = TEX_NOT_OK
	steps += 1
	blink = true


func _test() -> bool:
	var value: int = Cpu.outputs[output]
	return value >= min_requirement and value <= max_requirement

func test() -> bool:
	#prints("Test", name)
	if not _test():
		failed = true
		blink = false
		Symbol.texture.region.position.x = TEX_FAIL
		Symbol.show()
		#print("FAIL")
		return false
	#print("OK")
	return true
		
		
		

func _on_blinker_timeout() -> void:
	if not blink:
		return
	Symbol.visible = not Symbol.visible
	
