@tool
extends IOModule

@onready var Fluid: ColorRect = $Fluid
var temperatur: int = 0:
	set(new_temperatur):
		if new_temperatur == temperatur:
			return
		assert(new_temperatur >= 0 and new_temperatur <= 255)
		temperatur = new_temperatur
		Fluid.custom_minimum_size.y = temperatur / 5.3125
		#celsius = remap(temperatur, 0.0, 255.0, -26.0, 100.0)
		if temperatur <= 52:
			Fluid.color = Color(0.666, 0.666, 0.666)
		else:
			Fluid.color = Color(0.333, 0.333, 0.333)
		Cpu.set_input(input, temperatur)
		#prints("Temp", celsius, temperatur)

@export var celsius: int = 0:
	set(new_celsius):
		if new_celsius == celsius:
			return
		assert(new_celsius >= -26 and new_celsius <= 100)
		celsius = new_celsius
		temperatur = int(remap(celsius, -26, 100, 0, 255))

func set_celsius(value: int) -> void:
	celsius = value
