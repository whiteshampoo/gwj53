extends IOModule

@onready var Indicators: Array = [
	$Up, $Left, $Down, $Right
]

@export var keys: PackedStringArray = [
	"", "", "", "",
]

@export var upper: bool = false

func init() -> void:
	assert(Indicators.size() == 4 and keys.size() == 4)
	for i in keys.size():
		keys[i] = "I" + keys[i]
		Indicators[i].hide()
	


func _input(_event: InputEvent) -> void:
	if Cpu.state == CPU.STATE.DEVELOPING:
		return
	for i in keys.size():
		if Input.is_action_just_pressed(keys[i]):
			Cpu.set_input_bit(input, i + 4 * int(upper), true)
			Indicators[i].show()
		if Input.is_action_just_released(keys[i]):
			Cpu.set_input_bit(input, i + 4 * int(upper), false)
			Indicators[i].hide()
		
