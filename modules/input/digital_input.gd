extends IOModule

const NOTHING: int = 0
const HOT: int = 48
const COLD: int = 96

@onready var Visual: TextureRect = $Visual

func reset() -> void:
	nothing()

func hot() -> void:
	Visual.texture.region.position.x = HOT
	Cpu.set_input(input, 0x0F)

func cold() -> void:
	Visual.texture.region.position.x = COLD
	Cpu.set_input(input, 0xF0)

func nothing() -> void:
	Visual.texture.region.position.x = NOTHING
	Cpu.set_input(input, 0x00)
