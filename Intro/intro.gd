@tool
extends ColorRect

@export var start_colors: ColorSetting = null
@export var overlay_x: float = 0.0:
	set(x):
		overlay_x = x
		#Overlay.set_deferred("size", Vector2(x, 270.0))
		Overlay.anchor_right = x
@export var chars: int = 0:
	set(c):
		chars = c
		$Text.visible_characters = c

func _ready() -> void:
	print("Oh, you started me on the console.")
	print("Well, there is not TOO much to see here.")
	print("I hope you will enjoy me little game :)")
	if Engine.is_editor_hint():
		set_process(false)
		return
	Overlay.colors = start_colors
	$AnimationPlayer.play("Intro")


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("intro_skip"):
		overlay_x = 1.0
		_on_animation_player_animation_finished("")


func _on_animation_player_animation_finished(_anim_name: StringName) -> void:
	if Engine.is_editor_hint():
		return
	get_tree().change_scene_to_file("res://Main.tscn")
