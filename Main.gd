
extends ColorRect

const CPU_SETUP_DONE: String = "CPU setup done"

@onready var Code: CodeEdit = %Code
@onready var Cpu: CPU = %CPU
@onready var InfoBox: Label = %InfoBox
@onready var LevelDiv: HBoxContainer = %LevelDiv
@onready var Program: PopupMenu = $MenuDivider/Menu/Program

@onready var Credits: RichTextLabel = $Credits
@onready var Instructions: RichTextLabel = $Instructions
@onready var MenuDivider: VBoxContainer = $MenuDivider

var LVL: Level = null

func _ready() -> void:
#	if not DirAccess.dir_exists_absolute("user://solutions/"):
#		DirAccess.make_dir_absolute("user://solutions/")
#	
	for file in DirAccess.open("res://level/solutions/").get_files():
		#DirAccess.copy_absolute("res://level/solutions/%s" % file, "user://solution_%s" % file)
		var infile: FileAccess = FileAccess.open("res://level/solutions/%s" % file, FileAccess.READ)
		var outfile: FileAccess = FileAccess.open("user://solution_%s" % file, FileAccess.WRITE)
		outfile.store_string(infile.get_as_text())
	randomize()
	load_level("res://level/level_1.tscn")
	get_tree().get_root().min_size = Vector2i(480, 270)


func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventKey:
		return
	if not event.pressed:
		return
	if not Input.is_anything_pressed():
		return
#	if Input.is_action_just_pressed("fullscreen"):
#		if DisplayServer.window_get_mode(0) == DisplayServer.WINDOW_MODE_FULLSCREEN:
#			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
#		else:
#			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	if Input.is_action_just_pressed("run"):
		run()
	if Input.is_action_just_pressed("reset"):
		reset()
	if Input.is_action_just_pressed("step"):
		step()
		


func load_level(path: String) -> void:
	InfoBox.text = ""
	if is_instance_valid(LVL):
		Code.save_code(LVL.scene_file_path)
		LVL.queue_free()
	LVL = load(path).instantiate()
	LevelDiv.add_child(LVL)
	#LVL.tree_exiting.connect(Code.save_code.bind(path))
	LVL.message.connect(InfoBox.add_line)
	LVL.finished.connect(step)
	Code.max_lines = 1000 if LVL.name.contains("Playground") else 20
	Code.load_code(path)
	reset()


func run() -> void:
	match Cpu.state:
		CPU.STATE.RUNNING:
			return
		CPU.STATE.STEPPING:
			Cpu.state = CPU.STATE.RUNNING
			InfoBox.add_line("Continue")
			Program.set_state(CPU.STATE.RUNNING)
			Code.ignore_input = true
		CPU.STATE.DEVELOPING:
			var ret: String = setup_cpu()
			InfoBox.add_line(ret)
			if ret == CPU_SETUP_DONE:
				Cpu.state = CPU.STATE.RUNNING
				InfoBox.add_line("Running")
				Program.set_state(CPU.STATE.RUNNING)
				Code.ignore_input = true
				


func reset() -> void:
	for line in LVL.text.split("\n"):
		InfoBox.add_line(line)
	if Cpu.state == CPU.STATE.DEVELOPING:
		return
	Cpu.state = CPU.STATE.DEVELOPING
	Cpu.reset()
	InfoBox.add_line("CPU reset")
	LVL.reset()
	Program.set_state(CPU.STATE.DEVELOPING)
	Code.ignore_input = false


func step() -> void:
	match Cpu.state:
		CPU.STATE.RUNNING:
			Cpu.state = CPU.STATE.STEPPING
			InfoBox.add_line("Stop")
			Program.set_state(CPU.STATE.STEPPING)
			Code.ignore_input = true
			return
		CPU.STATE.STEPPING:
			Cpu.step()
		CPU.STATE.DEVELOPING:
			var ret: String = setup_cpu()
			InfoBox.add_line(ret)
			if ret == CPU_SETUP_DONE:
				Cpu.state = CPU.STATE.STEPPING
				InfoBox.add_line("Stepping")
				Program.set_state(CPU.STATE.STEPPING)
				Code.ignore_input = true
				Code.set_p_line(0)


func setup_cpu() -> String:
	Code.to_upper()
	var new_lines: Dictionary = Cpu.parse_asm(Code.text)
	if CPU.ERROR in new_lines:
		Cpu.state = CPU.STATE.DEVELOPING
		return new_lines[CPU.ERROR]
#	for line in new_lines[CPU.CMD].values():
#		print(line)
	Cpu.reset()
	Cpu.instruction_lines = new_lines[CPU.CMD]
	Cpu.labels = new_lines[CPU.LABELS]
	return CPU_SETUP_DONE

func _on_button_pressed() -> void:
	Cpu.step()


func _on_cpu_p_changed(new_value) -> void:
	Code.set_p_line(new_value)




func _on_level_change_level(path) -> void:
	load_level(path)



func _on_cpu_input_changed(_new_inputs) -> void:
	if not Program.is_item_checked(Program.BREAK):
		return
	InfoBox.add_line("Input has been changed")
	step()


func _on_instructions_back() -> void:
	Instructions.hide()
	MenuDivider.show()


func _on_help_instructions() -> void:
	Instructions.show()
	MenuDivider.hide()


func _on_credits_back() -> void:
	Credits.hide()
	MenuDivider.show()


func _on_help_credits() -> void:
	Credits.show()
	MenuDivider.hide()


func _on_tree_exiting() -> void:
	Code.save_code(LVL.scene_file_path)
