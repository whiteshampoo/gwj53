class_name Level
extends Panel

signal message(text)
signal finished

@onready var Grid: GridContainer = $Center/Grid
@export_node_path(CPU) var cpu_nodepath: NodePath = "../CPU"
@export_multiline var text: String = ""

var outputs: Array = Array()
var inputs: Array = Array()
var failed: bool = false

@onready var Cpu: CPU = get_node_or_null(cpu_nodepath)
@onready var Anim: AnimationPlayer = $AnimationPlayer

func reset() -> void:
	if Anim.has_animation("Level"):
		Anim.play("Level")
		Anim.seek(0.0)
	failed = false
	for child in Grid.get_children():
		if child.has_method("reset"):
			child.reset()

func _ready() -> void:
	assert(Cpu)
	assert(Grid.get_child_count() == 4)
	assert(get_node_or_null(cpu_nodepath))
	Cpu.stepped.connect(cpu_step)
	for child in Grid.get_children():
		assert(child is IOModule, str(child))
		child.Cpu = Cpu
		child.init()
	if Anim.has_animation("Level"):
		Anim.play("Level")


func cpu_step() -> void:
	if Anim.is_playing():
		Anim.advance(0.01)


	for child in Grid.get_children():
		if child is OutputModule:
			child.step()


func test_outputs() -> void:
	for child in Grid.get_children():
		if child is OutputModule:
			if not child.test():
				failed = true
				message.emit("%s failed test (%d)" % [
					child.output_name,
					Cpu.outputs[child.output],
				])
				message.emit("min: %d | max: %d" % [
					child.min_requirement,
					child.max_requirement,
				])


func _on_animation_player_animation_finished(_anim_name: StringName) -> void:
	message.emit("Simulation ended. Result:")
	if failed:
		message.emit("FAILED")
		message.emit("Please improve your code")
	else:
		message.emit("SUCCESS")
		message.emit("Please advance")
	finished.emit()
